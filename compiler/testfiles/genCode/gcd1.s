.global _start
_start:
	b main
	mov     %r0, $0
	mov     %r7, $1
	swi     $0
gcd1:
	push {fp}
	mov fp, sp
	mov r1, #8fp
	push {r1}
	mov r1, #16fp
	push {r1}
	mov r1, 16(%rbp)
	push {r1}
	mov r1, 24(%rbp)
	mov r0, r1
	pop {r1}
	cmp r1, r0
	bne while_gcd1_0_begin
	b while_gcd1_0_end
	mov sp, fp
	pop {fp}
	ret
while_gcd1_0_begin:
	mov r1, 16(%rbp)
	push {r1}
	mov r1, 24(%rbp)
	mov r0, r1
	pop {r1}
	cmp r1, r0
	bgt if_gcd1_0_0_begin
else_gcd1_0_0_begin:
	mov r1, 24(%rbp)
	push {r1}
	mov r1, 16(%rbp)
	mov r0, r1
	pop {r1}
	sub r1, r0
	mov 24(%rbp), r1
	b if_gcd1_0_0_end
	mov sp, fp
	pop {fp}
	ret
if_gcd1_0_0_begin:
	mov r1, 16(%rbp)
	push {r1}
	mov r1, 24(%rbp)
	mov r0, r1
	pop {r1}
	sub r1, r0
	mov 16(%rbp), r1
	b if_gcd1_0_0_end
if_gcd1_0_0_end:
	mov r1, 16(%rbp)
	push {r1}
	mov r1, 24(%rbp)
	mov r0, r1
	pop {r1}
	cmp r1, r0
	bne while_gcd1_0_begin
while_gcd1_0_end:
	mov r1, 16(%rbp)
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
gcd2:
	push {fp}
	mov fp, sp
	mov r1, #8fp
	push {r1}
	mov r1, #16fp
	push {r1}
	mov r1, 24(%rbp)
	push {r1}
	mov r1, #0
	mov r0, r1
	pop {r1}
	cmp r1, r0
	beq if_gcd2_0_begin
	b if_gcd2_0_end
	mov sp, fp
	pop {fp}
	ret
if_gcd2_0_begin:
	mov r1, 16(%rbp)
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
	b if_gcd2_0_end
if_gcd2_0_end:
	mov r1, 16(%rbp)
	push {r1}
	mov r1, 24(%rbp)
	mov r0, r1
	pop {r1}
	mov r2, #0
	idiv r0
	sub r1, r2
	push {r1}
	mov r1, 24(%rbp)
	push {r1}
	bl gcd2
	add sp, #16
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
main:
	push {fp}
	mov fp, sp
	sub sp, #24
	mov -8(%rbp), #0
	mov -16(%rbp), #0
	mov r1, #24
	mov -8(%rbp), r1
	mov r1, #20
	mov -16(%rbp), r1
	mov r1, -16(%rbp)
	push {r1}
	mov r1, -8(%rbp)
	push {r1}
	bl gcd1
	add sp, #16
	push {r1}
	bl print
	add sp, #8
	mov r1, -16(%rbp)
	push {r1}
	mov r1, -8(%rbp)
	push {r1}
	bl gcd2
	add sp, #16
	push {r1}
	bl print
	add sp, #8
	mov r1, -16(%rbp)
	push {r1}
	mov r1, -8(%rbp)
	push {r1}
	bl gcd1
	add sp, #16
	push {r1}
	mov r1, -16(%rbp)
	push {r1}
	mov r1, -8(%rbp)
	push {r1}
	bl gcd2
	add sp, #16
	mov r0, r1
	pop {r1}
	sub r1, r0
	mov r1, -24(%rbp), r1
	mov r1, -24(%rbp)
	push {r1}
	bl print
	add sp, #8
	mov r1, #0
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
