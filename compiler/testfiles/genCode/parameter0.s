.global _start
_start:
	b main
	mov     %r0, $0
	mov     %r7, $1
	swi     $0
f:
	push {fp}
	mov fp, sp
	mov r1, #8fp
	push {r1}
	mov r1, #16fp
	push {r1}
	mov r1, 16(%rbp)
	push {r1}
	mov r1, 24(%rbp)
	mov r0, r1
	pop {r1}
	mul r1, r0
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
main:
	push {fp}
	mov fp, sp
	mov r1, #5
	push {r1}
	mov r1, #2
	push {r1}
	bl f
	add sp, #16
	push {r1}
	bl print
	add sp, #8
	mov r1, #0
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
