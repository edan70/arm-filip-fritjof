import java.util.Collections;
import java.util.*;
aspect CodeGenARM {

    public void Program.genArm(PrintStream out) {
        /* Assembly generation starting point */
		
		// Define hardware
        out.println("\t.cpu cortex-a53"); /* this is the cpu of RBPi 3B*/
        out.println("\t.fpu neon-fp-armv8");
        out.println("\t.syntax unified\n"); /* modern syntax */
	
        out.println("\t.text");
        out.println("\t.align  2");
        out.println("\t.global main");

		// Generate helper functions
		sleepArmHelper(out);
		ledArmHelper(out);	
		printArmHelpers(out);
		readArmHelper(out);

		// Generate program
    	for (FunctionDeclaration decl : getFunctionDeclarationList()) {
		    decl.genArm(out);
	    }
    }

    public void Program.ledArmHelper(PrintStream out) {
		/* Generate function led, wrapper for changing color of led pin on RPi Sense HAT */

		/* file location for writing to LED lights */
		out.println("\t.section .rodata");
		out.println("\t.align 2");
		out.println("led_adress:");
		out.println("\t.ascii \"/dev/fb1\\000\"");
		out.println("\t.text");
		out.println("\t.align 2");

		/* function info */
		out.println("\t.global led");
		out.println("\t.syntax unified");
		out.println("\t.arm");
		out.println("\t.fpu vfp");
		out.println("\t.type	led, %function");

		/* led function */
		out.println("led:");
		out.println("\tpush	{lr}");
		out.println("\tpush	{fp}");
		out.println("\tadd	fp, sp, 4");
		out.println("\tsub	sp, sp, 96");
		out.println("\tldr	r1, [fp, 4]");
		out.println("\tstr	r1, [fp, -88]");
		out.println("\tldr	r1, [fp, 8]");
		out.println("\tstr	r1, [fp, -92]");
		out.println("\tmov	r1, 2");
		out.println("\tldr	r0, formatted_led_adress");
		out.println("\tbl	open");
		out.println("\tstr	r0, [fp, -8]");
		out.println("\tsub	r3, fp, 84");
		out.println("\tmov	r2, r3");
		out.println("\tldr	r1, formatted_led_adress+4");
		out.println("\tldr  r0, [fp, -8]");
		out.println("\tbl	ioctl");
		out.println("\tmov	r3, 0");
		out.println("\tstr	r3, [sp, 4]");
		out.println("\tldr	r3, [fp, -8]");
		out.println("\tstr	r3, [sp]");
		out.println("\tmov	r3, 1");
		out.println("\tmov	r2, 3");
		out.println("\tmov	r1, 128");
		out.println("\tmov	r0, 0");
		out.println("\tbl	mmap");
		out.println("\tstr	r0, [fp, -12]");
		out.println("\tldr	r3, [fp, -12]");
		out.println("\tstr	r3, [fp, -16]");
		out.println("\tldr	r3, [fp, -88]");
		out.println("\tlsl	r3, r3, 1");
		out.println("\tldr	r2, [fp, -16]");
		out.println("\tadd	r3, r2, r3");
		out.println("\tldr	r2, [fp, -92]");
		out.println("\tuxth	r2, r2");
		out.println("\tstrh	r2, [r3]");
		out.println("\tmov	r1, 128");
		out.println("\tldr	r0, [fp, -12]");
		out.println("\tbl	munmap");
		out.println("\tldr	r0, [fp, -8]");
		out.println("\tbl	close");
		out.println("\tmov	r3, 0");
		out.println("\tmov	r0, r3");
		out.println("\tsub	sp, fp, 4");
		out.println("\tpop	{fp}");
		out.println("\tpop	{lr}");
		out.println("\tbx	lr");

		/* Formatting for led file location */
		out.println("\t.align 2");
		out.println("formatted_led_adress:");
		out.println("\t.word led_adress");
		out.println("\t.word 17922");
		out.println("\t.size .led, .-led");

    }

    public void Program.readArmHelper(PrintStream out) {
		/* Function read, wrapper for scanf to use with numbers */
		
		out.println("format: .asciz \"%d\\000\"");
		out.println("\t.align 2\n");	

		out.println("read:");
		out.println("\tpush {lr}");
		out.println("\tpush {fp}");
		out.println("\tadd fp, sp, 0"); 		// set fp
		out.println("\tsub sp, sp, 4");			// make room for local variable
		out.println("\tsub r1, fp, 4");			// set r0 to adress in memory (int pointer)
		out.println("\tldr r0, addr_format");   // load formatting string
		out.println("\tbl scanf");				// call scanf
		out.println("\tldr r0, [fp, -4]");		// put value on addres in r0
		out.println("\tsub sp, fp, 0"); 		// restore stack pointer
		out.println("\tpop {fp}");
		out.println("\tpop {lr}");
		out.println("\tbx lr\n");

		/* formatting */
		out.println("addr_format: .word format");
		out.println("\t.align 2\n");	
    }

    public void Program.printArmHelpers(PrintStream out) {
        /* function print, which is a wrapper for printf to use with numbers */

        /* ascii formatting for printing integers */
        out.println("message:");
	    out.println("\t.ascii \"%d\\012\\000\"");   	// %d and newline
        out.println("formatted_message:");
        out.println("\t.word message");                 // message is a word

		/* function print */
		out.println("print:");                          // define print
		out.println("\tpush {lr}");						// save lr
		out.println("\tpush {fp}");						// save fp
		out.println("\tadd fp, sp, 0"); 				// set fp
		out.println("\tldr r1, [fp, 8]");               // load int arg from stack to r1
		out.println("\tldr r0, formatted_message");     // move message formatting to r0
		out.println("\tbl  printf");                    // call printf with args r1 and r0
		out.println("\tmov r0, 0");                     // mov return value 0 to r0 (return register)
		out.println("\tpop {fp}");						// restore fp
		out.println("\tpop {lr}");						// restore lr
		out.println("\tbx  lr");						// branch back to lr
    }

    public void Program.sleepArmHelper(PrintStream out) {
		/* function wait, wrapper for sleep function */
	
		out.println("wait:");
		out.println("\tpush {lr}");
		out.println("\tpush {fp}");
		out.println("\tadd fp, sp, 0");
		out.println("\tldr r0, [fp, 8]"); // load input from memory
		out.println("\tbl sleep");
		out.println("\tmov r0, 0");
		out.println("\tpop {fp}");
		out.println("\tpop {lr}");
		out.println("\tbx lr");
    }

    public void FunctionDeclaration.genArm(PrintStream out) {
		out.println(getname().getID() + ":");
		out.println("\tpush {lr}");                 		// save caller frame pointer (don't touch lr)
		out.println("\tpush {fp}");
		out.println("\tadd fp, sp, 0");                     // set new frame pointer
		int nbrLocals = getblock().numLocals();
		int nbrArgs = getargs().getArgList().getNumChild();
		int bytesOfSpace = (nbrLocals + nbrArgs)*4 + 4;	    // calculate needed memory
		if (bytesOfSpace > 0) {
			out.println("\tsub sp, " + bytesOfSpace);       // allocate memory for local variables + args
		}
		int nbrProcessed = 1;
		for (Arg arg : getargs().getArgList()) {
			out.println("\tldr r1, [fp, " + (4 + 4*nbrProcessed) + "]");      	// move argument to r1
			out.println("\tstr r1, " + arg.getname().arm_address());    		// store r1 on new local address
			nbrProcessed += 1;
		}

		getblock().genArm(out);		// gen code for function block 
	}

    public void Statement.genArm(PrintStream out){

    }


    public void Block.genArm(PrintStream out){
        for (Statement stmt : getStatementList()) {
            stmt.genArm(out);
        }
    }

    public void Negation.genArm(PrintStream out){
        getexpression().genArmNeg(out);
    }

    public void ReturnStatement.genArm(PrintStream out){
        getExpression().genArm(out);
        out.println("\tmov r0, r1");    // return values go in r0
		out.println("\tsub sp, fp, 0"); // delete allocated memory
		out.println("\tpop {fp}");
		out.println("\tpop {lr}");
		out.println("\tbx lr");
    }

    public void Expression.genArm(PrintStream out){

    }

    public void Expression.genArmNeg(PrintStream out){

    }


    public void Numeral.genArm(PrintStream out){
        out.println("\tmov r1, " + getNUMERAL()); // move numeral to register 1
    }

    public void Numeral.genArmNeg(PrintStream out){
        out.println("\tmov r1, -" + getNUMERAL()); // move negative numeral to register 1
    }

    public void FunctionCallStatement.genArm(PrintStream out){
        getfunction().genArm(out);
    }

    public void IdUse.genArm(PrintStream out){
        out.println("\tldr r1, " + decl().arm_address()); // load variable from memory
    }

    public void FunctionCall.genArm(PrintStream out){
        int nbrExpr = getParams().getNumExpression();
        for (int i = nbrExpr-1; i >= 0; i--) {
            getParams().getExpression(i).genArm(out);
            out.println("\tpush {r1}"); 			// push argument
        }
        out.println("\tbl " + getname().getID());	// call function
		out.println("\tadd sp, " + nbrExpr*4);		// restore/remove arguments by moving stack pointer
   		out.println("\tmov r1, r0");				// move return value to r1 
   }

    public void Sub.genArm(PrintStream out) {
        getLeft().genArm(out);
        out.println("\tpush {r1}");
        getRight().genArm(out);
        out.println("\tmov r0, r1");
        out.println("\tpop {r1}");
        out.println("\tsub r1, r0");
    }

    public void Plus.genArm(PrintStream out) {
        getLeft().genArm(out);
        out.println("\tpush {r1}");
        getRight().genArm(out);
        out.println("\tmov r0, r1");
        out.println("\tpop {r1}");
        out.println("\tadd r1, r0");
    }

    public void Mod.genArm(PrintStream out) {
        getLeft().genArm(out);
        out.println("\tpush {r1}");
        getRight().genArm(out);
        out.println("\tmov r0, r1");
        out.println("\tpop {r1}");
        out.println("\tsdiv r2, r1, r0");	// divide r1 by r0 and put result in r2
		out.println("\tmul r3, r2, r0");	// multiply result of division with divider
		out.println("\tsub r2, r1, r3");	// subtact
        out.println("\tmov r1, r2"); 		// move value in r3 to r1
    }

    public void Eq.genArm(PrintStream out){
        getLeft().genArm(out);
        out.println("\tpush {r1}");
        getRight().genArm(out);
        out.println("\tmov r0, r1");
        out.println("\tpop {r1}");
        out.println("\tcmp r1, r0");
        out.print("\tbeq "); // branch if equal
    }

    public void Noteq.genArm(PrintStream out){
        getLeft().genArm(out);
        out.println("\tpush {r1}");
        getRight().genArm(out);
        out.println("\tmov r0, r1");
        out.println("\tpop {r1}");
        out.println("\tcmp r1, r0");
        out.print("\tbne "); // branch if not equal
    }

    public void Gt.genArm(PrintStream out){
        getLeft().genArm(out);
        out.println("\tpush {r1}");
        getRight().genArm(out);
        out.println("\tmov r0, r1");
        out.println("\tpop {r1}");
        out.println("\tcmp r1, r0");
        out.print("\tbgt "); // branch if greater than
    }

    public void Gteq.genArm(PrintStream out){
        getLeft().genArm(out);
        out.println("\tpush {r1}");
        getRight().genArm(out);
        out.println("\tmov r0, r1");
        out.println("\tpop {r1}");
        out.println("\tcmp r1, r0");
        out.print("\tbge "); // branch if greater or equal
    }

    public void Le.genArm(PrintStream out){
        getLeft().genArm(out);
        out.println("\tpush {r1}");
        getRight().genArm(out);
        out.println("\tmov r0, r1");
        out.println("\tpop {r1}");
        out.println("\tcmp r1, r0");
        out.print("\tblt "); // branch if lower than
    }

    public void Leq.genArm(PrintStream out){
        getLeft().genArm(out);
        out.println("\tpush {r1}");
        getRight().genArm(out);
        out.println("\tmov r0, r1");
        out.println("\tpop {r1}");
        out.println("\tcmp r1, r0");
        out.print("\tble "); // branch if lower or equal
    }


    public void Mul.genArm(PrintStream out) {
        getLeft().genArm(out);
        out.println("\tpush {r1}");
        getRight().genArm(out);
        out.println("\tmov r0, r1");
        out.println("\tpop {r1}");
        out.println("\tmul r1, r0");
    }

    public void Div.genArm(PrintStream out) {
        getLeft().genArm(out);
        out.println("\tpush {r1}");
        getRight().genArm(out);
        out.println("\tmov r0, r1");
        out.println("\tpop {r1}");
        out.println("\tsdiv r1, r0");
    }

    public void Declaration.genArm(PrintStream out){
        if(hasExpression()){
            getExpression().genArm(out);
            out.println("\tstr r1, " + getIdDecl().arm_address()); // intialize with value in r1 (value from expression)
        } else {
	    	out.println("\tmov r1, 0");
            out.println("\tstr r1, " + getIdDecl().arm_address()); // declared with no value => initialized with value 0
        }
    }

    public void Assignment.genArm(PrintStream out){
        getexpression().genArm(out);
        out.println("\tstr r1, " + getname().decl().arm_address());
    }

    public void IfStatement.genArm(PrintStream out){
        getExpression().genArm(out);
        out.println("if_" + uniqueIfWhileNum()+"begin");
        if(hasElse()){
            out.println("else_" + uniqueIfWhileNum() + "begin:");
            getElse().genArm(out);
        }
        out.println("\tb " + "if_" + uniqueIfWhileNum() + "end");
		
        out.println("\tmov sp, fp");
        out.println("\tpop {fp}");
        out.println("\tbx lr");
		
        out.println("if_" + uniqueIfWhileNum() + "begin:");
        getThen().genArm(out);
        out.println("\tb " + "if_" + uniqueIfWhileNum() + "end");
        out.println("if_" + uniqueIfWhileNum() + "end:");
    }

    public void WhileStatement.genArm(PrintStream out){
        getExpression().genArm(out);
        out.println("while_" + uniqueIfWhileNum()+"begin");
        out.println("\tb " + "while_" + uniqueIfWhileNum() + "end");
		
        out.println("\tmov sp, fp");
        out.println("\tpop {fp}");
        out.println("\tbx lr");
		
        out.println("while_" + uniqueIfWhileNum() + "begin:");
        getDo().genArm(out);
        getExpression().genArm(out);
        out.println(""+"while_" + uniqueIfWhileNum() + "begin");
        out.println("while_" + uniqueIfWhileNum() + "end:");
    }


    syn String IdDecl.arm_address(){
        return "[fp, -" + localIndex()*4 + "]"; // arm memory formatting
    }


}
