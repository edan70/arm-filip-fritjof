package lang;

import java.io.File;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lang.ast.Program;

@RunWith(Parameterized.class)
public class InterpreterTests {
    private final String inFile;
    private static final File TEST_DIRECTORY = new File("testfiles/Interpreter");
    public InterpreterTests(String testFile) {
		inFile = testFile;
	}
    @Test public void runTest() throws Exception {
        PrintStream out = System.out;
        try {
            Program program = (Program) Util.parse(new File(TEST_DIRECTORY, inFile));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            System.setOut(new PrintStream(baos));
            if(program.errors().size() == 0){
                 program.eval();
            }
            String actual = baos.toString();
            Util.compareOutput(actual,
			new File(TEST_DIRECTORY, Util.changeExtension(inFile, ".out")),
			new File(TEST_DIRECTORY, Util.changeExtension(inFile, ".expected")));
	      
        } finally {
            System.setOut(out);
        }
    }   

    @Parameters(name = "{0}")
		public static Iterable<Object[]> getTests() {
			return Util.getTestParameters(TEST_DIRECTORY, ".in");
	} 
}