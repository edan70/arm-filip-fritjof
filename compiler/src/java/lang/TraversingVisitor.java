package lang;

import lang.ast.*;

/**
 * Traverses each node, passing the data to the children.
 * Returns the data unchanged.
 * Overriding methods may change the data passed and the data returned.
 */
public abstract class TraversingVisitor implements lang.ast.Visitor {

	protected Object visitChildren(ASTNode node, Object data) {
		for (int i = 0; i < node.getNumChild(); ++i) {
			node.getChild(i).accept(this, data);
		}
		return data;
	}

	public Object visit(List node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Opt node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Program node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Declaration node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Mul node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Div node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Numeral node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(IdDecl node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(IdUse node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(FunctionDeclaration node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(FunctionCallStatement node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Arg node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Args node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(FunctionCall node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Param node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Params node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Block node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Assignment node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(IfStatement node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(WhileStatement node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(ReturnStatement node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Mod node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Plus node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Sub node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Le node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Leq node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Gt node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Gteq node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Eq node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Noteq node, Object data) {
		return visitChildren(node, data);
	}
	public Object visit(Negation node, Object data) {
		return visitChildren(node, data);
	}

	// public Object visit(Id node, Object data) {
	// 	return visitChildren(node, data);
	// }


	// public Object visit(Binding node, Object data) {
	// 	return visitChildren(node, data);
	// }

	// public Object visit(Ask node, Object data) {
	// 	return visitChildren(node, data);
	// }
}
