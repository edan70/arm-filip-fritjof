.global _start
_start:
	b main
	mov     %r0, $0
	mov     %r7, $1
	swi     $0
main:
	push {fp}
	mov fp, sp
	mov r1, #5
	push {r1}
	mov r1, #3
	mov r0, r1
	pop {r1}
	sub r1, r0
	push {r1}
	bl print
	add sp, #8
	mov r1, #0
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
