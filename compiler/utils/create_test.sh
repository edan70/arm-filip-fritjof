#!/bin/bash

TESTNAME="$1"

mv "${PWD}/testfiles/ast/${TESTNAME}.out" "${PWD}/testfiles/ast/${TESTNAME}.expected"
