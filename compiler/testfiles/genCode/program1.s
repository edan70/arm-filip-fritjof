.global _start
_start:
	b main
	mov     %r0, $0
	mov     %r7, $1
	swi     $0
main:
	push {fp}
	mov fp, sp
	mov r1, #1
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
