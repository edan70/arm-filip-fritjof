package lang;

import lang.ast.*;

public class MsnVisitor extends TraversingVisitor {
    
    private static int depth;
    private static int maxDepth;

    public static int result(ASTNode n) {
        depth = 0;
        maxDepth = 0;
        MsnVisitor m = new MsnVisitor();
        n.accept(m, null);
        return maxDepth;
    }

    public Object visit(WhileStatement s, Object data) {
        depth += 1;
        super.visit(s, data);
        if (depth > maxDepth) maxDepth = depth;
        depth -= 1;
        return data;
    }

    public Object visit(IfStatement s, Object data) {
        depth += 1;
        super.visit(s, data);
        if (depth > maxDepth) maxDepth = depth;
        depth -= 1;
        return data;
    }

    public Object visit(FunctionDeclaration s, Object data) {
        depth += 1;
        super.visit(s, data);
        if (depth > maxDepth) maxDepth = depth;
        depth -= 1;
        return data;
    }
}
