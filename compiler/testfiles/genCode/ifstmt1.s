.global _start
_start:
	b main
	mov     %r0, $0
	mov     %r7, $1
	swi     $0
main:
	push {fp}
	mov fp, sp
	mov r1, #1
	push {r1}
	mov r1, #2
	mov r0, r1
	pop {r1}
	cmp r1, r0
	beq if_main_0_begin
	b if_main_0_end
	mov sp, fp
	pop {fp}
	ret
if_main_0_begin:
	mov r1, #5
	push {r1}
	mov r1, #2
	mov r0, r1
	pop {r1}
	cmp r1, r0
	bgt while_main_0_0_begin
	b while_main_0_0_end
	mov sp, fp
	pop {fp}
	ret
while_main_0_0_begin:
	mov r1, #5
	push {r1}
	bl print
	add sp, #8
	mov r1, #5
	push {r1}
	mov r1, #2
	mov r0, r1
	pop {r1}
	cmp r1, r0
	bgt while_main_0_0_begin
while_main_0_0_end:
	mov r1, #4
	push {r1}
	mov r1, #3
	mov r0, r1
	pop {r1}
	cmp r1, r0
	bgt while_main_0_1_begin
	b while_main_0_1_end
	mov sp, fp
	pop {fp}
	ret
while_main_0_1_begin:
	mov r1, #11
	push {r1}
	bl print
	add sp, #8
	mov r1, #4
	push {r1}
	mov r1, #3
	mov r0, r1
	pop {r1}
	cmp r1, r0
	bgt while_main_0_1_begin
while_main_0_1_end:
	b if_main_0_end
if_main_0_end:
	mov r1, #5
	push {r1}
	mov r1, #3
	mov r0, r1
	pop {r1}
	cmp r1, r0
	bge if_main_1_begin
	b if_main_1_end
	mov sp, fp
	pop {fp}
	ret
if_main_1_begin:
	mov r1, #5
	push {r1}
	mov r1, #5
	mov r0, r1
	pop {r1}
	cmp r1, r0
	beq while_main_1_0_begin
	b while_main_1_0_end
	mov sp, fp
	pop {fp}
	ret
while_main_1_0_begin:
	mov r1, #10
	push {r1}
	bl print
	add sp, #8
	mov r1, #5
	push {r1}
	mov r1, #5
	mov r0, r1
	pop {r1}
	cmp r1, r0
	beq while_main_1_0_begin
while_main_1_0_end:
	b if_main_1_end
if_main_1_end:
	mov r1, #0
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
