#!/bin/bash

FROM="${1}"
TO="${2}"

for f in *."${FROM}"; do
    mv -- "$f" "$(basename -- "$f" .$FROM).$TO"
done
