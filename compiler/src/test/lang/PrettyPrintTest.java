package lang;

import java.io.File;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lang.ast.Program;

@RunWith(Parameterized.class)
public class PrettyPrintTest {
	private final String filename;
	/** Directory where the test input files are stored. */
	private static final File TEST_DIRECTORY = new File("testfiles/prettyPrint");
	public PrettyPrintTest(String testFile) {
		filename = testFile;
	  }

	@Test public void runTest() throws Exception {
		Program program = (Program) Util.parse(new File(TEST_DIRECTORY, filename));
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		program.prettyPrint(new PrintStream(bytes));
		String actual = bytes.toString();
		Util.compareOutput(actual,
			new File(TEST_DIRECTORY, Util.changeExtension(filename, ".out")),
			new File(TEST_DIRECTORY, Util.changeExtension(filename, ".expected")));
	  }
	
	@Parameters(name = "{0}")
		public static Iterable<Object[]> getTests() {
			return Util.getTestParameters(TEST_DIRECTORY, ".in");
	}
}
