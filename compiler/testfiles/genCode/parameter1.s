.global _start
_start:
	b main
	mov     %r0, $0
	mov     %r7, $1
	swi     $0
log2:
	push {fp}
	mov fp, sp
	sub sp, #8
	mov r1, #8fp
	push {r1}
	mov r1, #0
	mov r1, -16(%rbp), r1
	mov r1, 16(%rbp)
	push {r1}
	mov r1, #1
	mov r0, r1
	pop {r1}
	cmp r1, r0
	bgt while_log2_1_begin
	b while_log2_1_end
	mov sp, fp
	pop {fp}
	ret
while_log2_1_begin:
	mov r1, 16(%rbp)
	push {r1}
	mov r1, #2
	mov r0, r1
	pop {r1}
	div r1, r0
	mov 16(%rbp), r1
	mov r1, -16(%rbp)
	push {r1}
	mov r1, #1
	mov r0, r1
	pop {r1}
	add r1, r0
	mov -16(%rbp), r1
	mov r1, 16(%rbp)
	push {r1}
	mov r1, #1
	mov r0, r1
	pop {r1}
	cmp r1, r0
	bgt while_log2_1_begin
while_log2_1_end:
	mov r1, -16(%rbp)
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
main:
	push {fp}
	mov fp, sp
	sub sp, #8
	bl read
	add sp, #0
	mov r1, -8(%rbp), r1
	mov r1, -8(%rbp)
	push {r1}
	bl log2
	add sp, #8
	push {r1}
	bl print
	add sp, #8
	mov r1, #0
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
