.global _start
_start:
	b main
	mov     %r0, $0
	mov     %r7, $1
	swi     $0
f:
	push {fp}
	mov fp, sp
	mov r1, #2
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
main:
	push {fp}
	mov fp, sp
	bl f
	add sp, #0
	mov r1, #1
	push {r1}
	mov sp, fp
	pop {fp}
	pop {pc}
