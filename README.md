# JastArm

[![License](https://img.shields.io/badge/License-BSD_2--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

JastArm is a continuation of the compiler developed during the course EDAN65 - Compilers, given at Lund University. That compiler generates and assembles programs written in `SimpliC` for the `x86` architecture. It makes use of the meta-compilation system `JastAdd`, also developed at Lund University.

`SimpliC` is a small C like language, with emphasis on small.

JastArm extends the compiler with the possibility to also compile programs for the `arm` architecture.

Another extension is the addition of a `led()` function; it is used to access the led matrix on the [Raspberry Pi Sense HAT](https://projects.raspberrypi.org/en/projects/getting-started-with-the-sense-hat/2). 

The `led()` function takes two arguments, index and color. The first argument is the index of the matrix you want to access, from 0 to 63. The second argument is an integer value representing the color you want to set that index to. For example, if we wanted to set the 10th led to red we would call:

```
int red = 60000;
led(10, red);
```

There is a larger example of how the `led()` function is used located under `/compiler/tree.simplic`. This is a small program that generates an awesome Christmas tree on the Sense HAT 🎄. 

_Note_: We have not figured out what integer corresponds to what color. Our best tip is trial and error. If you figure it out, feel free to submit a pull request.

Further reading:
    
* [JastAdd](https://jastadd.cs.lth.se/web/index.php)
* [EDAN65](https://kurser.lth.se/lot/?val=kurs&kurskod=EDAN65&lang=en)

---

## How to use this repo

_Requirements:_ 
* Generating assembly: `java`
* Generating binary and running: `gcc`

__NOTE__: As to this point and during development the generated assembly code has _only_ been run on a Raspberry Pi 3 with an `arm` CPU. You will __NOT__ be able to run the binaries on an `x86` CPU. However, you can still generate `arm` assembly, just not run it.

1. Clone the repo

```
$ git clone git@bitbucket.org:edan70/arm-filip-fritjof.git jastarm
$ ls
> jastarm
```

2. Build jar file using gradle.

```
$ cd jastarm/compiler
$ ./gradlew jar
```

3. If the compiler is built successfully there should not be any errors. Gradle might produce some warnings though, as the project is WIP.

```
$ ls | grep compiler.jar
> compiler.jar
```

4. Generate assembly for the example program `HelloJastArm.simplic`. This is a tiny program that prints the [greatest common divisor](https://en.wikipedia.org/wiki/Greatest_common_divisor) between 24 and 20. It uses two different methods labeled `gcd1` and `gcd2` for this. It then prints the difference between the results of these methods, and then exits with code 0.

```
$ java -jar compiler.jar HelloJastArm.simplic > HelloJastArm.s
$ ls | grep HelloJastArm.s
> HelloJastArm.s
```

5. Assemble, link and run the assembly code with `gcc`.

```
$ gcc -gstabs HelloJastArm.s
$ ./a.out
> 0
$ echo $?
> 0
```

### Some notes

* Build properties for gradle are located in `build.gradle`
* Right now `JastArm` does not produce the binary, that is the reason for steps 4 and 5.
* For testing purposes `JastArm` produces assembly code to `stdout`, that is why need to send the output to `HelloJastArm.s` manually in step 4.

## Source code

If you want to contribute to the project we recommend starting in `path/to/jastarm/compiler/src/jastadd/codeGenArm.jrag`

This is the `JastAdd` aspect that generates the ARM instructions.

It might also be interesting to look at `path/to/.../jastadd/CodeGenHelpers.jrag`

## License

This repository is covered by the license BSD 2-clause, see file LICENSE.
