import java.io.PrintStream;

aspect PrettyPrint {
	public void ASTNode.prettyPrint(PrintStream out) {
		prettyPrint(out, "");
		out.println();
	}

	public void ASTNode.prettyPrint(PrintStream out, String ind) {
	  for (ASTNode child : astChildren()) {
		  child.prettyPrint(out, ind);
		}
	}

	public void Program.prettyPrint(PrintStream out, String ind) {
		int nbrOfChildren = getNumFunctionDeclaration();
		if(nbrOfChildren > 0){
			for(FunctionDeclaration funcDecl : getFunctionDeclarationList()){
				funcDecl.prettyPrint(out, ind);
			}
		}
	}

	public void FunctionDeclaration.prettyPrint(PrintStream out, String ind) {
		out.print("int ");
		getChild(0).prettyPrint(out, ind);
		getChild(1).prettyPrint(out, ind);
		getChild(2).prettyPrint(out, ind);
	}

	public void FunctionCallStatement.prettyPrint(PrintStream out, String ind) {
		out.print(ind);
		getfunction().prettyPrint(out, ind);
		out.print("(");
		out.print(");");
	}

	public void Arg.prettyPrint(PrintStream out, String ind) {
		getname().prettyPrint(out, ind);
	}

	public void Args.prettyPrint(PrintStream out, String ind) {
		out.print("(");
		if(getNumArg() > 0){
			List argList = getArgList();
			int nbrOfChildren = getNumArg();
			if(nbrOfChildren > 0){
				for(int i=0; i < nbrOfChildren; i++){
					out.print("int ");
					argList.getChild(i).prettyPrint(out, ind);
					if(i < nbrOfChildren-1){
						out.print(", ");
					}
				}
				
			}
		}
		out.print(") ");
	}

	public void FunctionCall.prettyPrint(PrintStream out, String ind) {
		getname().prettyPrint(out, ind);
		getParams().prettyPrint(out, ind);
	}

	public void Param.prettyPrint(PrintStream out, String ind) {
		getval().prettyPrint(out, ind);
	}

	public void Params.prettyPrint(PrintStream out, String ind) {
		List paramList = getExpressionList();
		int length = getNumExpression();
		if(length > 0){
			out.print("(");
			for(int i=0; i < length; i++){
				paramList.getChild(i).prettyPrint(out, ind);
				if(length > 1 && i < length-1){
					out.print(",");
				}
			}
			out.print(")");
		}
	}

	public void Block.prettyPrint(PrintStream out, String ind) {
		out.print("{");
		if(getNumStatement() == 0){
			out.println();
		}
		if(getNumStatement() > 0){
			for(Statement stmt : getStatementList()){
				out.println();
				stmt.prettyPrint(out, ind + "    ");
			}
		}
		if(getNumStatement() != 0){
			out.println();
		}
		out.print(ind + "}");
	}

	public void Assignment.prettyPrint(PrintStream out, String ind) {
			out.print(ind);
			getname().prettyPrint(out, ind);
			out.print(" = ");
			getexpression().prettyPrint(out, ind);
			out.print(";");
	}

	public void Declaration.prettyPrint(PrintStream out, String ind){
		out.print(ind + "int ");
		getIdDecl().prettyPrint(out, ind);
		if(hasExpression()){
			out.print(" = ");
			getExpression().prettyPrint(out, ind);
		}
		out.print(";");
	}

	public void IfStatement.prettyPrint(PrintStream out, String ind) {
		out.print(ind + "if (");
		getChild(0).prettyPrint(out, ind);
		out.print(") ");
		getChild(1).prettyPrint(out, ind);
		if(hasElse()){
			out.print(" else ");
			getChild(2).prettyPrint(out, ind);
		}
	}

	public void WhileStatement.prettyPrint(PrintStream out, String ind) {
		out.print(ind + "while (");
		getChild(0).prettyPrint(out, ind);
		out.print(") ");
		getChild(1).prettyPrint(out, ind);
	}

	public void ReturnStatement.prettyPrint(PrintStream out, String ind) {
		out.print(ind + "return ");
		getExpression().prettyPrint(out, ind);
		out.print(";");
	}

	public void Mul.prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" * ");
		getRight().prettyPrint(out, ind);
	}

	public void Div.prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" / ");
		getRight().prettyPrint(out, ind);
	}

	public void Plus.prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" + ");
		getRight().prettyPrint(out, ind);
	}

	public void Sub.prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" - ");
		getRight().prettyPrint(out, ind);
	}

	public void Mod.prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" % ");
		getRight().prettyPrint(out, ind);
	}

	public void Le.prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" < ");
		getRight().prettyPrint(out, ind);
	}

	public void Leq.prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" <= ");
		getRight().prettyPrint(out, ind);
	}

	public void Gt.prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" > ");
		getRight().prettyPrint(out, ind);
	}

	public void Gteq.prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" >= ");
		getRight().prettyPrint(out, ind);
	}

	public void Eq.prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" == ");
		getRight().prettyPrint(out, ind);
	}

	public void Noteq.prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" != ");
		getRight().prettyPrint(out, ind);
	}

	public void Numeral.prettyPrint(PrintStream out, String ind) {
		out.print(getNUMERAL());
	}

	public void IdUse.prettyPrint(PrintStream out, String ind) {
		out.print(getID());
	}

	public void IdDecl.prettyPrint(PrintStream out, String ind) {
		out.print(getID());
	}

	public void Negation.prettyPrint(PrintStream out, String ind) {
		out.print(getexpression());
	}

	// public void Id.prettyPrint(PrintStream out, String ind) {

	// 	out.print(getID());
	// }
}
